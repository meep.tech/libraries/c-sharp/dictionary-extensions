﻿using System.Collections.Generic;
using System.Linq;

public static partial class DictionaryExtensions {
  /// <summary>
  /// Quick access tripple dic for caches
  /// </summary>
  public class TrippleKeyDictionary<TKey1, TKey2, TKey3, TValue>: Dictionary<TKey1, Dictionary<TKey2, Dictionary<TKey3, TValue>>> {
    public TValue this[TKey1 key1, TKey2 key2, TKey3 key3] {
      get => TryGetValue(key1, out var key2Dic)
        ? key2Dic.TryGetValue(key2, out var key3Dic)
          ? key3Dic.TryGetValue(key3, out var value)
            ? value
            : default
          : default
        : default;
      set {
        if (!ContainsKey(key1)) {
          this[key1] = new Dictionary<TKey2, Dictionary<TKey3, TValue>>();
        }
        if (!this[key1].ContainsKey(key2)) {
          this[key1][key2] = new Dictionary<TKey3, TValue>();
        }
        this[key1][key2][key3] = value;
      }
    }

    /// <summary>
    /// Remove and clean up for keys
    /// </summary>
    public TValue RemoveAt(TKey1 key1, TKey2 key2, TKey3 key3) {
      if (TryGetValue(key1, out var key2Dic)) {
        if (key2Dic.TryGetValue(key2, out var key3Dic)) {
          if (key3Dic.TryGetValue(key3, out var value)) {
            key3Dic.Remove(key3);

            if (!key3Dic.Any()) {
              key2Dic.Remove(key2);
            }
            if (!this.Any()) {
              Remove(key1);
            }

            return value;
          }
        }
      }

      return default;
    }

    /// <summary>
    /// Remove and clean up for keys
    /// </summary>
    public void Remove(TKey1 key1, TKey2 key2, TKey3 key3) {
      if (TryGetValue(key1, out var key2Dic)) {
        if (key2Dic.TryGetValue(key2, out var key3Dic)) {
          if (key3Dic.TryGetValue(key3, out var value)) {
            key3Dic.Remove(key3);

            if (!key3Dic.Any()) {
              key2Dic.Remove(key2);
            }
            if (!this.Any()) {
              Remove(key1);
            }
          }
        }
      }
    }
  }
}