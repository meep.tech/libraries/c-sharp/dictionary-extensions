﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static partial class DictionaryExtensions {

  /// <summary>
  /// Deconstruct a key value pair:
  /// </summary>
  public static void Deconstruct<TKey, TValue>(
      this KeyValuePair<TKey, TValue> kvp,
      out TKey key,
      out TValue value
  ) {
    key = kvp.Key;
    value = kvp.Value;
  }

  /// <summary>
  /// Set all the values for each key in a dictionary to a specific value
  /// </summary>
  public static void SetAllValuesTo<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TValue newValue) {
    foreach(TKey key in dictionary.Keys.ToList()) {
      dictionary[key] = newValue;
    }
  }

  /// <summary>
  /// Merge dictionaries together, overriding any values with the same key.
  /// </summary>
  public static Dictionary<TKey, TValue> Merge<TKey, TValue>(this Dictionary<TKey, TValue> baseDict, params Dictionary<TKey, TValue>[] dictionariesToMergeIn) {
    if (dictionariesToMergeIn == null) return baseDict;
    Dictionary<TKey, TValue> result = baseDict;
    foreach (Dictionary<TKey, TValue> dictionary in dictionariesToMergeIn) {
      foreach (KeyValuePair<TKey, TValue> entry in dictionary) {
        result[entry.Key] = entry.Value;
      }
    }
    return result;
  }

  /// <summary>
  /// Merge dictionaries together, overriding any values with the same key.
  /// </summary>
  public static IDictionary Merge(this IDictionary baseDict, params IDictionary[] dictionariesToMergeIn) {
    if (dictionariesToMergeIn == null) return baseDict;
    IDictionary result = baseDict;
    foreach (IDictionary dictionary in dictionariesToMergeIn) {
      foreach (object key in dictionary.Keys) {
        result[key] = dictionary[key];
      }
    }
    return result;
  }

  /// <summary>
  /// Merge dictionaries together, overriding any values with the same key.
  /// </summary>
  public static IDictionary MergeRecursively(this IDictionary baseDict, params IDictionary[] dictionariesToMergeIn) {
    if (dictionariesToMergeIn == null) return baseDict;
    IDictionary result = baseDict;
    foreach (IDictionary dictionary in dictionariesToMergeIn) {
      foreach (object key in dictionary.Keys) {
        if (result.Contains(key) && dictionary[key] is IDictionary newDic && result[key] is IDictionary existingDic) {
          existingDic.MergeRecursively(newDic);
        } else
        result[key] = dictionary[key];
      }
    }
    return result;
  }

  /// <summary>
  /// Merge dictionaries together, overriding any values with the same key.
  /// </summary>
  public static Dictionary<TKey, TValue> MergeRecursively<TKey, TValue>(this Dictionary<TKey, TValue> baseDict, params Dictionary<TKey, TValue>[] dictionariesToMergeIn) {
    if (dictionariesToMergeIn == null) return baseDict;
    Dictionary<TKey, TValue> result = baseDict;
    foreach (Dictionary<TKey, TValue> dictionary in dictionariesToMergeIn) {
      foreach (KeyValuePair<TKey, TValue> entry in dictionary) {
        if (result.ContainsKey(entry.Key) && entry.Value is IDictionary newDic && result[entry.Key] is IDictionary existingDic) {
          existingDic.MergeRecursively(newDic);
        } else
        result[entry.Key] = entry.Value;
      }
    }
    return result;
  }

  /// <summary>
  /// Merge multiple dictionaries containing sub dictionaries of collections together into the first one.
  /// </summary>
  public static Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>> Merge<TKey, TKey2, TValue>(
    this Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>> baseDict,
    params Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>>[] dictionariesToMergeIn
  ) {
    if (dictionariesToMergeIn == null) return baseDict;
    foreach (Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>> collectionOfDictionaries in dictionariesToMergeIn) {
      foreach (KeyValuePair<TKey, Dictionary<TKey2, IEnumerable<TValue>>> childDictionaryCollectionsToAddByFirstKey in collectionOfDictionaries) {
        if (baseDict.TryGetValue(childDictionaryCollectionsToAddByFirstKey.Key, out Dictionary<TKey2, IEnumerable<TValue>> existingChildDictionaryCollectionForFirstKey)) {
          foreach (KeyValuePair<TKey2, IEnumerable<TValue>> dictionaryEntriesToAddBySecondKey in childDictionaryCollectionsToAddByFirstKey.Value) {
            if (existingChildDictionaryCollectionForFirstKey.TryGetValue(dictionaryEntriesToAddBySecondKey.Key, out IEnumerable<TValue> existingChildDictionaryCollectionForSecondKey)) {
              existingChildDictionaryCollectionForFirstKey[dictionaryEntriesToAddBySecondKey.Key] = existingChildDictionaryCollectionForSecondKey.Concat(dictionaryEntriesToAddBySecondKey.Value);
            } else {
              existingChildDictionaryCollectionForFirstKey[dictionaryEntriesToAddBySecondKey.Key] = dictionaryEntriesToAddBySecondKey.Value;
            }
          }
        } else {
          baseDict[childDictionaryCollectionsToAddByFirstKey.Key] = childDictionaryCollectionsToAddByFirstKey.Value;
        }
      }
    }

    return baseDict;
  }

  /// <summary>
  /// Merge multiple dictionaries containing sub dictionaries of collections together into the first one.
  /// </summary>
  public static Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>> MergeAnd<TKey, TKey2, TValue>(
    this Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>> baseDict,
    Func<TValue, TValue> @do,
    params Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>>[] dictionariesToMergeIn
  ) {
    if (dictionariesToMergeIn == null) return baseDict;
    foreach (Dictionary<TKey, Dictionary<TKey2, IEnumerable<TValue>>> collectionOfDictionaries in dictionariesToMergeIn) {
      foreach (KeyValuePair<TKey, Dictionary<TKey2, IEnumerable<TValue>>> childDictionaryCollectionsToAddByFirstKey in collectionOfDictionaries) {
        if (baseDict.TryGetValue(childDictionaryCollectionsToAddByFirstKey.Key, out Dictionary<TKey2, IEnumerable<TValue>> existingChildDictionaryCollectionForFirstKey)) {
          foreach (KeyValuePair<TKey2, IEnumerable<TValue>> dictionaryEntriesToAddBySecondKey in childDictionaryCollectionsToAddByFirstKey.Value) {
            if (existingChildDictionaryCollectionForFirstKey.TryGetValue(dictionaryEntriesToAddBySecondKey.Key, out IEnumerable<TValue> existingChildDictionaryCollectionForSecondKey)) {
              existingChildDictionaryCollectionForFirstKey[dictionaryEntriesToAddBySecondKey.Key] = existingChildDictionaryCollectionForSecondKey.Concat(dictionaryEntriesToAddBySecondKey.Value.Select(@do));
            } else {
              existingChildDictionaryCollectionForFirstKey[dictionaryEntriesToAddBySecondKey.Key] = dictionaryEntriesToAddBySecondKey.Value.Select(@do);
            }
          }
        } else {
          baseDict[childDictionaryCollectionsToAddByFirstKey.Key] = childDictionaryCollectionsToAddByFirstKey.Value.ToDictionary(item => item.Key, item => item.Value.Select(@do));
        }
      }
    }

    return baseDict;
  }

  /// <summary>
  /// Merge dictionaries together, overriding any values with the same key.
  /// </summary>
  public static Dictionary<TKey, TValue> Merge<TKey, TValue>(this Dictionary<TKey, TValue> baseDict, params IReadOnlyDictionary<TKey, TValue>[] dictionariesToMergeIn) {
    if (dictionariesToMergeIn == null) return baseDict;
    Dictionary<TKey, TValue> result = baseDict;
    foreach (Dictionary<TKey, TValue> dictionary in dictionariesToMergeIn) {
      foreach (KeyValuePair<TKey, TValue> entry in dictionary) {
        result[entry.Key] = entry.Value;
      }
    }
    return result;
  }

  /// <summary>
  /// Merge dictionaries together, overriding any values with the same key.
  /// </summary>
  public static Dictionary<TKey, TValue> Merge<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> baseDict, params Dictionary<TKey, TValue>[] dictionariesToMergeIn) {
    Dictionary<TKey, TValue> results = baseDict.ToDictionary(x => x.Key, y => y.Value);
    if (dictionariesToMergeIn == null) return results;
    Dictionary<TKey, TValue> result = results;
    foreach (Dictionary<TKey, TValue> dictionary in dictionariesToMergeIn) {
      foreach (KeyValuePair<TKey, TValue> entry in dictionary) {
        result[entry.Key] = entry.Value;
      }
    }
    return result;
  }

  /// <summary>
  /// Add an item to a ICollection within a dictionary at the given key
  /// </summary>
  public static void AddToValueCollection<TKey, TValue>(this Dictionary<TKey, ICollection<TValue>> dictionary, TKey key, TValue value) {
    if (dictionary.TryGetValue(key, out ICollection<TValue> valueCollection)) {
      valueCollection.Add(value);
    } else dictionary.Add(key, new List<TValue> { value });
  }

  /// <summary>
  /// Remove an item from an ICollection within a dictionary at the given key
  /// </summary>
  public static bool RemoveFromValueCollection<TKey, TValue>(this Dictionary<TKey, ICollection<TValue>> dictionary, TKey key, TValue value) {
    if (dictionary.TryGetValue(key, out ICollection<TValue> valueCollection)) {
      return valueCollection.Remove(value);
    } else return false;
  }

  /// <summary>
  /// Do something for each dictionary entry
  /// </summary>
  /// <typeparam name="K"></typeparam>
  /// <typeparam name="V"></typeparam>
  /// <param name="enumeration"></param>
  /// <param name="do"></param>
  public static void ForEach<K, V>(this Dictionary<K, V> enumeration, Action<K, V> @do) {
    foreach (var thing in enumeration) {
      @do(thing.Key, thing.Value);
    }
  }
}