﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// A dictionary with an ordered queue as well as accessable key elements.
/// Should be thread safe
/// </summary>
public class QueueDictionary<TKey, TValue> : IDictionary<TKey, TValue> {

  /// <summary>
  /// Underlying queue
  /// </summary>
  private readonly LinkedList<KeyValuePair<TKey, TValue>> _queue =
    new LinkedList<KeyValuePair<TKey, TValue>>();

  /// <summary>
  /// Underlying dictionary
  /// </summary>
  private readonly Dictionary<TKey, LinkedListNode<KeyValuePair<TKey, TValue>>>
    _dictionary = new Dictionary<TKey, LinkedListNode<KeyValuePair<TKey, TValue>>>();

  /// <summary>
  /// Underlying sync lock
  /// </summary>
  private readonly object _syncRoot
    = new object();

  /// <summary>
  /// Get the next element that will be dequeued
  /// </summary>
  public TValue Next
    => _queue.FirstOrDefault().Value;

  /// <summary>
  /// Empty ctor
  /// </summary>
  public QueueDictionary() { }

  /// <summary>
  /// Create this from an existing dictionary
  /// </summary>
  /// <param name="keyValuePairs"></param>
  public QueueDictionary(IEnumerable<KeyValuePair<TKey, TValue>> keyValuePairs) {
    foreach((TKey key, TValue value) in keyValuePairs) {
      Add(key, value);
    }
  }

  /// <summary>
  /// Remove the next item from the queue
  /// </summary>
  /// <returns></returns>
  public TValue Dequeue() {
    lock (_syncRoot) {
      KeyValuePair<TKey, TValue> item = _queue.First();
      _queue.RemoveLast();
      _dictionary.Remove(item.Key);
      return item.Value;
    }
  }

  /// <summary>
  /// Remove an item from the queue by key
  /// </summary>
  /// <param name="key"></param>
  /// <returns></returns>
  public TValue Dequeue(TKey key) {
    lock (_syncRoot) {
      LinkedListNode<KeyValuePair<TKey, TValue>> node = _dictionary[key];
      _dictionary.Remove(key);
      _queue.Remove(node);
      return node.Value.Value;
    }
  }

  /// <summary>
  /// Add an item to the queue
  /// </summary>
  /// <param name="key"></param>
  /// <param name="value"></param>
  public void Enqueue(TKey key, TValue value) {
    lock (_syncRoot) {
      LinkedListNode<KeyValuePair<TKey, TValue>> node =
        _queue.AddFirst(new KeyValuePair<TKey, TValue>(key, value));
      _dictionary.Add(key, node);
    }
  }

  #region IDictionary

  public TValue this[TKey key] {
    get => _dictionary[key].Value.Value;
    set {
      // update if exists
      if (ContainsKey(key)) {
        _dictionary[key].Value = new KeyValuePair<TKey, TValue>(key, value);
      } // else just enque it as new 
      else {
        Enqueue(key, value);
      }
    }
  }

  public ICollection<TKey> Keys
    => _dictionary.Keys;

  public ICollection<TValue> Values
    => _dictionary.Values.Select(linkedListItem => linkedListItem.Value.Value).ToList();

  public int Count
    => _queue.Count;

  public bool IsReadOnly
    => false;

  public void Add(TKey key, TValue value)
    => Enqueue(key, value);

  public void Add(KeyValuePair<TKey, TValue> item)
    => Enqueue(item.Key, item.Value);

  public void Clear() {
    _dictionary.Clear();
    _queue.Clear();
  }

  public bool Contains(KeyValuePair<TKey, TValue> item)
    => _dictionary.TryGetValue(item.Key, out var node)
      ? node.Value.Value.Equals(item.Value)
      : false;

  public bool ContainsKey(TKey key)
    => _dictionary.ContainsKey(key);

  public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) {
    _dictionary.Select(entry => new KeyValuePair<TKey, TValue>(entry.Key, entry.Value.Value.Value)).ToList().CopyTo(array, arrayIndex);
  }

  public bool Remove(TKey key) {
    if (ContainsKey(key)) {
      Dequeue(key);
      return true;
    }

    return false;
  }

  public bool Remove(KeyValuePair<TKey, TValue> item)
    => Remove(item.Key);

  public bool TryGetValue(TKey key, out TValue value)
    => (value = ContainsKey(key)
      ? this[key]
      : default
    ) != null;

  public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    => _queue.GetEnumerator();

  IEnumerator IEnumerable.GetEnumerator()
    => GetEnumerator();

  #endregion
}